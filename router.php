<?php



if($_SERVER['REQUEST_URI'] == "/slendoJSON/router.php/tasks"){

	$date = new DateTime();
	echo  '[
			  {
			    "id": 1,
			    "name": "Faire les courses",
			    "date":"'. date_format($date ,"Y-m-d").'",
            	"hour": "'. date_format(
            				date_add($date, date_interval_create_from_date_string('10 secondes'))
            		,"G:i:s").'"
			    , "realise": 0
			},
			    {
			    "id": 2,
			    "name": "Aller chercher les enfants",
			    "date":"'. date_format($date ,"Y-m-d").'",
            	"hour": "'. date_format(
            				date_add($date, date_interval_create_from_date_string('2 hour'))
            		,"G:i:s").'"
			    , "realise": 0
			},
			    {
			    "id": 3,
			    "name": "Aller voir le garagiste",
			    "date":"'. date_format(
            				date_add($date, date_interval_create_from_date_string('-1 day'))
            		,"Y-m-d").'",

            	"hour": "'. date_format(
            		date_add(
            				date_add($date, date_interval_create_from_date_string('1 day')), 
            				date_interval_create_from_date_string('+3 hour'))
            		,"G:i:s") .'"
			    , "realise": 0
			},
			    {
			    "id": 4,
			    "name": "Réparer le bureau du salon",
			    "date":"'. date_format(
            				date_add($date, date_interval_create_from_date_string('1 day'))
            		,"Y-m-d").'",
            	"hour": "'. date_format(
            				date_add($date, date_interval_create_from_date_string('+3 hour'))
            		,"G:i:s").'"
			    , "realise": 0
			},
			    {
			    "id": 5,
			    "name": "Installer ampoule eco.",
			    "date":"'. date_format(
            				date_add($date, date_interval_create_from_date_string('2 day'))
            		,"Y-m-d").'",
            	"hour": "'. date_format(
            				date_add($date, date_interval_create_from_date_string('3 hour'))
            		,"G:i:s").'"
			    , "realise": 0
			},
			    {
			    "id": 6,
			    "name": "Vider le garage",
			    "date":"'. date_format(
            				date_add($date, date_interval_create_from_date_string('3 day'))
            		,"Y-m-d").'",
            	"hour": "'. date_format(
            				date_add($date, date_interval_create_from_date_string('3 day'))
            		,"G:i:s").'"
			    , "realise": 0
			},
			    {
			    "id": 7,
			    "name": "Réparer la radio",
			    "date":"'.date_format(
            				date_add($date, date_interval_create_from_date_string('6 day'))
            		,"Y-m-d").'",
            	"hour": "'. date_format(
            				date_add($date, date_interval_create_from_date_string('6 day'))
            		,"G:i:s").'"
			    , "realise": 0
			},
			    {
			    "id": 8,
			    "name": "Préparer thèse",
			    "date":"'. date_format(
            				date_add($date, date_interval_create_from_date_string('5 day'))
            		,"Y-m-d").'",
            	"hour": "'. date_format(
            				date_add($date, date_interval_create_from_date_string('5 day'))
            		,"G:i:s").'"
			    , "realise": 0
			}

]';

	/*End if*/
}


