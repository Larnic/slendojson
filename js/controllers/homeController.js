

   app.controller('homeController', homeController);

   function homeController($scope, $rootScope, $q, $interval, taskFactory, $filter, $timeout, $templateCache, SimpleTask){

         $rootScope.home = true;


         $scope.effectuee = function($id){
            $task = {};
            $task = taskFactory.getTaskById($id);
            $task.realise = 1;

            var promise = taskFactory.updateTask($task);
            $rootScope.ajaxSend = true;
            promise.then(
               function(response){
                  if(response.data['response']  == "true"){
                     var cle;
                  angular.forEach($rootScope.tachesB, function(value, key) {
                     if(value.id == $task.id){
                        cle = key;
                        }
                  }, []);
                  
                     $rootScope.tachesB.splice(cle, 1);

                     Materialize.toast('Tâche marquée comme effectuée', 3000, 'green center-align');
                  }

                     $rootScope.ajaxSend = false;
                     
               }

               , function (response) {
                   $rootScope.ajaxSend = false;
                   Materialize.toast('Une erreur est suvenue, veuillez réessayer ultérieurement', 3000, 'red accent-2 center-align');7
                 })
         }


         //template cache
         $templateCache.get('home.html');
   }