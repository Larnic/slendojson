



   app.controller('searchController', searchController);

   function searchController($scope, $rootScope, $filter, $templateCache, taskFactory, SimpleTask){

   		$rootScope.home = false;
   		$scope.tachesTrouvees = false;

   		$scope.search = function(){

   			$scope.tachesTrouvees = false;
   			var tampon= [];
        if(!angular.equals($scope.searchKey, "")){

     			angular.forEach($rootScope.taches, function(value, key) {
                  angular.forEach(value, function(valeur, keyValeur) {
                    if($filter('lowercase')(valeur.name).search($filter('lowercase')($scope.searchKey)) > -1){
  					         tampon.push(valeur);
  				}
                  }, []);
            }, []);

     			if(tampon.length > 0){
  				$scope.tachesTrouvees = tampon;
  			}
        }
   		}

   		 //template cache
         $templateCache.get('rechercher.html');

   }