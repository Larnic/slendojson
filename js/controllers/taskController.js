

app.controller("taskController", taskController);

function taskController($scope, $rootScope, taskFactory,  $routeParams, $location, $timeout, $templateCache, SimpleTask){

	$rootScope.home = false;
	var path = $location.path();
	$scope.modifiedTask = {};
	$scope.newTask = {};

	if(path.substr(0, 10) == "/modifier/"){
		$timeout(function(){
			angular.element('#hour').attr('type', "time");
		}, 600)
	}


	$scope.modifiedTask = taskFactory.getTask($routeParams.slug);
	$timeout(function(){
		angular.element("input[name=_submit]").val($scope.modifiedTask.date);
		
	},500);
	$scope.check = function(){

	if(angular.equals($scope.modifiedTask, {})){
		$location.path('/');
	}
		var check = false;
		var date = angular.element("input[name=_submit]").val();
		var newHour = $scope.getFormatedDate(date, $scope.modifiedTask.hour);
		if($scope.modifiedTask.name.length > 4 && $scope.modifiedTask.date.length >= 9
			&&
			newHour.length > 5){

			if(new Date(date+" "+angular.element("#hour").val()).getTime() >= new Date().getTime()){
					check = true;
			}
		}
		return check;
	}

	$scope.checkNew = function(){
		var check = false;

		var newHour = $scope.getFormatedDate($scope.newTask.date, $scope.newTask.hour);
		if(!angular.isUndefined($scope.newTask.name) && !angular.isUndefined($scope.newTask.date)){
			if($scope.newTask.name.length > 4 && $scope.newTask.date.length >= 9
				&&
				newHour.length >= 5){
				var date = new Date(angular.element("input[name=_submit]").val()+" "+angular.element("#hour").val());
				if(date >= new Date()){
					check = true;
				}
			}
		}
			return check;
	}

	$scope.updateTask = function(){

		if(angular.element("input[name=_submit]").val() !=""){
			$scope.modifiedTask.date = angular.element("input[name=_submit]").val();
		}

			var promise = taskFactory.updateTask($scope.modifiedTask);
			
			promise.then(
				function(response){
					if(response.data['response']  == "true"){
						var cle;
						$scope.modifiedTask.hour = $scope.convertFDate($scope.modifiedTask.hour)
					angular.forEach($rootScope.tachesB, function(value, key) {
                        if(value.id == $scope.modifiedTask.id){
                        	cle = key;
                           }


                  }, []);

                    $rootScope.tachesB[cle] = $scope.modifiedTask;

	   					Materialize.toast('Tâche mise à jour', 3000, 'green center-align');
					}
					
	   				$rootScope.ajaxSend = false;
	   				$location.path('/');	
				}

				, function (response) {
					$rootScope.ajaxSend = false;
				    Materialize.toast('Une erreur est suvenue, veuillez réessayer ultérieurement', 3000, 'red accent-2 center-align');
				  });
		}


		$scope.createTask = function(){

		$scope.newTask.date = angular.element("input[name=_submit]").val();
		$hour = $scope.newTask.hour.getHours()+":"+$scope.newTask.hour.getMinutes()+":"+$scope.newTask.hour.getSeconds();
		$scope.newTask.hourToSend = $scope.getFormatedDate($scope.newTask.date, $hour);
		$scope.newTask.realise = false;
		var promise = taskFactory.createTask($scope.newTask);
			
			promise.then(
				function(response){
					if(response.data['response']  == "true"){
	   					$scope.newTask.id = response.data['identifier'];
						$scope.newTask.hourJS = $scope.convertFDate($scope.getFormatedDate($scope.newTask.date, $scope.newTask.hourToSend));
	   					
						$rootScope.tachesB.push($scope.newTask);
							
        				
	   					Materialize.toast('Tâche créer avec succès', 3000, 'green center-align');
					}
					
	   				$scope.ajaxSend = false;
	   				$location.path('/');	
				}

				, function (response) {
					$scope.ajaxSend = false;
				    Materialize.toast('Une erreur est suvenue, veuillez réessayer ultérieurement', 3000, 'red accent-2 center-align');
				  });	

		}


		$scope.getFormatedDate = function($date, $hour){

			var dateFormated = "";
			if(!angular.isUndefined($hour)){
				if($hour.toString().length > 4 && !angular.isUndefined($date)){
					
					var date = new Date($date+" "+$hour);
					dateFormated = date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
				}
			}

			return dateFormated;
		}

		$scope.convertFDate = function($date){

			var date = new Date();
			date = date.getFullYear()+"/"+date.getMonth()+"/"+date.getDate();
			date = new Date(date+" "+$date);
			var hours = date.getHours().toString();
			var minutes = date.getMinutes().toString();
			var secondes = date.getSeconds().toString();

			if (hours.length == 1) {
			    hours = "0" + hours;
			}

			if (minutes.length == 1){
			    minutes = "0" + minutes;
			}

			if (secondes.length == 1){
			    secondes = "0" + secondes;
			}


			return hours+':'+ minutes+":"+secondes;
		}

		 //template cache
         $templateCache.get('creer.html');
         $templateCache.get('modifier.html');

}