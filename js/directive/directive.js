

app.directive('ngMenu', function () {
    return {
        restrict: 'A',
         controller: "homeController"
    }
});

app.directive('ngButtonCollapse', function () {
    return {
        restrict: 'A',
        link : function(scope, element, attrs){
        	element.sideNav(
        		{
        			closeOnClick: true
        		});
        }
    }
});


app.directive('ngShowMe', [ '$timeout', function ($timeout) {
    return {
        restrict: 'A',
        link : function(scope, element, attrs){

		    element.hover(function(e) {
		        var $id = element.parent().attr('id');
		    	angular.element("#"+$id+"-affect").slideDown(500);
		    });

		    element.parent().mouseleave(function(e) {
		    	$timeout(function() {

			        var $id = element.parent().attr('id');
			    	angular.element("#"+$id+"-affect").slideUp(500);
		    		
		    	}, 100);
		    });
        }
    }
}]);



app.directive('changeIt', ['myData', function(myData){
    return {
        restrict: 'C',
        link: function (scope, element, attrs) {
            scope.name = myData.name;
        }
    }
}]);


app.directive('ngDatePicker', function(){

	return {
		restrict: "A",
		controller: "taskController",
	
	link: function (scope, element, attrs) {

		var date = new Date();
			// Extension
		$.extend($.fn.pickadate.defaults, {
		  monthsFull: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
		  monthsShort: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
		  weekdaysShort: [ 'L', 'M', 'M', 'J', 'V', 'S', 'D' ],
		  weekdaysFull: ['Dimanche','Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		  show_months_full: false,
		  showMonthsShort: false,
		  formatSubmit: 'yyyy-mm-dd',
		  format: "d/mm/yyyy",
		  firstDay: 1,
		  today: "",
		  clear: '',
		  disable: [],
		  close: 'Fermer',
		   selectMonths: true, 
		   min: new Date(date.getFullYear(), date.getMonth(), date.getDate()),
		   max: new Date(date.getFullYear(), date.getMonth()+2, date.getDate()+6),
		   selectYears: true,
		});

		//Initialise Datepicker
		 element.pickadate({
				selectMonths: true, 
				selectYears: 15,
				closeOnSelect: true,
		 	onSet: function(event){
		 		if(event.select){
		 			this.close();
		 		}
		 	},
		 	onClose: function() {
			    element.blur();
			    element.blur();
			}
			});

        }
	}
})