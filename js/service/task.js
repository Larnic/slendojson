

app.factory('taskFactory', function(requestFactory, $rootScope, $q, $filter){

	var factory = {

		taches : false,

		getTasks : function(){

			var deferred = $q.defer();

			if($rootScope.taches !== false ){
				deferred.resolve({"data" : factory.taches});
				return deferred.promise;
			}
			else{

			return requestFactory.sendRequest(
				"GET", "router.php/tasks", {});
			}

		},

		removeTask: function($taskId){

			var deferred = $q.defer();
			deferred.resolve({'response': {'response': 'true'}});

			return deferred.promise;

			/*
			Pour fonctionnement avec un serveur
			return requestFactory.sendRequest(
				"DELETE", "/deleteTask", {
					taskId: $taskId
				});*/

		},

		getTask: function($slug){
		var task = {};
		angular.forEach($rootScope.taches, function(value, key) {
            angular.forEach( value, function(valeur, keyValeur) {
	           if($slug == $filter("slugFilter")(valeur.name)){
					task = Object.assign({}, valeur);
				}
            }, []);
      }, []);


			return task;
		},

		getTaskById: function($id){
		var task = {};
		angular.forEach($rootScope.tachesB, function(value, key) {
           if(value.id == $id){
				task = Object.assign({}, value);
			}
      	}, []);

			return task;
		},

		updateTask: function(taskToUpdate){

			var deferred = $q.defer();
			deferred.resolve({'data':  {'response': 'true'}});

			return deferred.promise;

			/*
			¨Fonctionnement avec un serveur web
			return requestFactory.sendRequest(
				"POST", "/updateTask", {
					tache: taskToUpdate
				});*/
		},

		createTask: function(taskToCreate){

			var deferred = $q.defer();
			deferred.resolve({'data': {'response': 'true'}});

			return deferred.promise;

			/*
			¨Fonctionnement avec un serveur web
			return requestFactory.sendRequest(
				"POST", "/createTask", {
					tache: taskToCreate
				});*/
		}
		
	};

	return factory;

});