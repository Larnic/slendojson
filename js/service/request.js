

app.factory('requestFactory', function($http, $cookies, CSRF_TOKEN){

	var factory = {


		sendRequest: function(method, url, params){
		 	
				/*if($cookies.get('XSRF-TOKEN') == undefined)
					{*/
					return $http({
						method: method,
						url: url,
						params: params,
						headers: {
							"X-CSRF-TOKEN": CSRF_TOKEN,
							'Content-Type' : 'application/x-www-form-urlencoded'
						}
					});
				/*}
				else{
					return $http({
						method: method,
						url: url,
						params: params
					});
				}*/

			}
	};

	return factory;

});