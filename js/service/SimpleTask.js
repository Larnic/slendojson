
app.factory('SimpleTask', function() {


    // instantiate initial object
    function SimpleTask($tache) {
        this.tache = $tache;
        this.tache = setSpace(this.tache);

        return this.tache;
    }

    var setSpace = function($tache){

           var $space = {
               1: {
                 text: "Aujourd'hui",
                 couleur: "orange accent-3"
              },
               2: {
                 text: "Demain",
                 couleur: "light-blue darken-1"
              },
               3: {
                 text: "Cette semaine",
                 couleur: "light-blue darken-1"
              },
              4: {
                 text: "Semaine prochaine et Plus",
                 couleur:  "light-blue darken-1"
              },
              5: {
                 text: "Non effectuée",
                 couleur:  "red"
              }
           }

        Date.prototype.getWeek = function() {
				  var date = new Date(this.getTime());
				   date.setHours(0, 0, 0, 0);
				  // Thursday in current week decides the year.
				  date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
				  // January 4 is always in week 1.
				  var week1 = new Date(date.getFullYear(), 0, 4);
				  // Adjust to Thursday in week 1 and count number of weeks from date to week1.
				  return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
				                        - 3 + (week1.getDay() + 6) % 7) / 7);
				}

           Date.prototype.addDays = function(date, days) {
			     var vretour = new Date(date);
			     vretour.setDate(vretour.getDate() + days);
			     return vretour;
		     }
              if(!angular.isUndefined($tache['hourJS'])){
                var date = new Date($tache['date']+" "+$tache['hourJS']);
              }
              else{
                var date = new Date($tache['date']+" "+$tache['hour']);
              }

               var aujourdhui = new Date();
               var numSemaine = aujourdhui.getWeek();
               var demain = aujourdhui.addDays(aujourdhui, 1);

                if($tache['realise'] == 0 && date.getTime() < aujourdhui.getTime()){
                	$tache.timeStamp = $space[5];
                  	return $tache;
                }

               if(angular.equals(date.toLocaleDateString(),aujourdhui.toLocaleDateString())){
                  $tache.timeStamp = $space[1];
                  return $tache;
               }
               else{
                  if(angular.equals(date.toLocaleDateString(), demain.toLocaleDateString())) {
                     $tache.timeStamp = $space[2];
                     return $tache;
                  }
                  else{
                     if(angular.equals(date.getMonth()+1 ,aujourdhui.getMonth()+1)
                        && date.getWeek() == numSemaine){
                        $tache.timeStamp = $space[3];
                        return $tache;
                     }
                     else{
                        $tache.timeStamp = $space[4];
                        return $tache;
                     }
                  }
               }            
         }

    return SimpleTask;
})