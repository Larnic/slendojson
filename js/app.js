
var app = angular.module('SlenApp', ['ngRoute', 'ngAnimate', 'ngCookies']);

		app.config(function($routeProvider, $locationProvider, $compileProvider){

			$routeProvider.when("/", {
				templateUrl: 'js/partials/home.html'
				,controller: 'homeController'
			})
			.when("/modifier/:slug", {
				templateUrl: 'js/partials/modifier.html',
				controller: 'taskController'
			})
			.when("/rechercher", {
				templateUrl: 'js/partials/rechercher.html',
				controller: 'searchController'
			})
			.when("/creer-une-tache", {
				templateUrl: 'js/partials/creer.html',
				controller: 'taskController'
			})
			.otherwise({redirectTo: '/'});

			$compileProvider.debugInfoEnabled(false);

		});

/********************** CSRF Protection ***********************/
angular.module("SlenApp").constant("CSRF_TOKEN", $('[name=_token]').val());

/******************** Global functionc ************************/

angular.module("SlenApp").run(['SimpleTask','taskFactory', '$timeout', '$interval', '$rootScope', function(SimpleTask, taskFactory, $timeout, $interval, $rootScope) {

  $rootScope.taches = false;
  $rootScope.home = true;
  $rootScope.ajouter = false;
  $rootScope.ajaxSend = true;
  $rootScope.audio = new Audio('sons/alarm.wav');

  var promise = taskFactory.getTasks();
      promise.then(
        function(response){
               var taches = [];

          if(response.data.length > 0){
                  angular.forEach(response.data, function(value, key) {
                     taches.push(SimpleTask(value));
                  }, []);
                     $rootScope.tachesB = response.data;
                     $rootScope.taches = $rootScope.Classer(taches);
                     $rootScope.verifyTask();
          }
          
            $rootScope.ajaxSend = false;  
        }

        , function (response) {
            $rootScope.ajaxSend = false;
            Materialize.toast('Une erreur est suvenue, veuillez réessayer ultérieurement', 3000, 'red accent-2 center-align');
          });

  $rootScope.alarme = false;

	$rootScope.deleteTask = function($id){

   			var promise = taskFactory.removeTask($id);
   			$rootScope.ajaxSend = true;
   			promise.then(
				function(response){   
   					$rootScope.ajaxSend = false;
   					if(response.data['response'] == "true"){
                     var compteur = 0;
   						angular.forEach($rootScope.taches, function(value, key) {
                        compteur = compteur+value.length;
                        angular.forEach( value, function(valeur, keyValeur) {
                           if(valeur.id == $id){
      								        value.splice(keyValeur, 1);
                               compteur = compteur-1;
                              Materialize.toast('Tâche supprimée', 3000, 'green center-align');
                           }

                        }, []);
                  }, []);

              angular.forEach($rootScope.tachesB, function(value, key) {
                        if(value.id == $id){
                              $rootScope.tachesB.splice(key, 1);
                           }
                  }, []);

                         if(compteur == 0){
                           $rootScope.taches = false;
                        }  
   					}
   					else{
   						Materialize.toast('Tâche non supprimée, réessayez', 3000, 'red accent-2 center-align');
   					}
				}

				, function (response) {
					$rootScope.ajaxSend = false; 
				    Materialize.toast('Une erreur est suvenue, veuillez réessayer ultérieurement', 3000, 'red accent-2 center-align');
				  });
   		}


   	$rootScope.verifyTask = function(){

   		var $stop = $interval(function() {
        var taches = [];
        angular.forEach($rootScope.tachesB, function(value, key) {
                     taches.push(SimpleTask(value));
                  }, []);


       $rootScope.taches = $rootScope.Classer(taches);
            
        angular.forEach($rootScope.taches, function(value, key) {
              angular.forEach(value, function(valeur, keyValeur) {
                var date = new Date().toString();
                var dateCompare = new Date(valeur['date']+" "+valeur['hour']).toString();
                
                 if(dateCompare == date){
                    $rootScope.audio.play();
                    angular.element("#alarme").css({"display": "table"});
                    angular.element("#alarme p").text("Rappel: "+valeur.name);
                    $rootScope.alarme = valeur;
                    var stop = $timeout(function(){
                       $rootScope.alarme = false;
                    }, 11000);
                 }
              }, []);
            }, []);
          }, 1000);
      }


      $rootScope.Classer = function($taches){

         var auj = [];
         var dem = [];
         var cetteSem = [];
         var semPep = [];
         var nonEf = [];

         angular.forEach($taches, function(value, key) {
            switch(value.timeStamp['text']) {
                case "Aujourd'hui":
                    auj.push(value);
                    break;
                case "Demain":
                    dem.push(value);
                    break;
               case "Cette semaine":
                    cetteSem.push(value);
                    break;
               case "Semaine prochaine et Plus":
                    semPep.push(value);
                    break;
               case "Non effectuée":
                    nonEf.push(value);
                    break;
            }

         }, []);

         if($taches.length == 0){
            return false;
          }

         return { "noneffectuee": nonEf, "aujourdhui": auj,
          "demain": dem, "cettesemaine": cetteSem, "semaineprochaine": semPep }

      }


}]);